import { Body, Delete, JsonController, Param, Post } from 'routing-controllers'
import { PassengersService } from '../services/passengers.service'

@JsonController('/passengers', { transformResponse: false })
export default class PassengersController {
    private _passengersService: PassengersService

    constructor() {
        this._passengersService = new PassengersService()
    }

    @Post('')
    async addPassengerToFlight(
        @Body()
        passenger: {
            code: string
            name: string
            flightCode: string
        }
    ) {
        return {
            status: 200,
            data: await this._passengersService.addPassengerToFlight(passenger),
        }
    }

    @Delete('/:code')
    async removerPassengerFromFlight(@Param('code') code: string) {
        return {
            status: 200,
            // data: await this._passengersService.addPassengerToFlight(passenger),
        }
    }
}
