import { DatabaseInstanceStrategy } from '../database'
import { Database } from '../databases/database_abstract'
import { FlightsService } from './flights.service'

export class PassengersService {
    private readonly _db: Database
    private readonly _flightsService: FlightsService

    constructor() {
        this._db = DatabaseInstanceStrategy.getInstance()
        this._flightsService = new FlightsService()
    }

    public async addPassengerToFlight(passenger: {
        code: string
        name: string
        flightCode: string
    }) {
        const allFlights: Array<any> = await this._flightsService.getFlights()
        const flight = allFlights.find((f) => f.code === passenger.code)
        if (!flight) {
            throw new Error('Flight not found')
        }

        const currentPassengers = JSON.parse(flight.passengers)

        if (currentPassengers.contains(passenger.code)) {
            throw new Error('Passenger already on flight')
        }

        currentPassengers.push(passenger.code)

        // await this._flightsService.addFlight()

        // return this._db.getFlights()
    }
}
